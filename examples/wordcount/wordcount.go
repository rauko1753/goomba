package main

import (
	"strconv"
	"strings"

	"gitlab.com/rauko1753/goomba/mr"
)

func TokenCount(input mr.KeyValue, inter chan mr.KeyValue) {
	for _, token := range strings.Fields(input.Value) {
		inter <- mr.KeyValue{token, "1"}
	}
}

func Sum(inter mr.KeyValues, output chan string) {
	sum := 0
	for val := range inter.Values {
		count, err := strconv.Atoi(val)
		if err != nil {
			panic(err)
		}
		sum += count
	}
	output <- strconv.Itoa(sum)
}

func main() {
	mr.Program{
		Mapper:  TokenCount,
		Reducer: Sum,
	}.Run()
}
