Goomba
======

Goomba is (or rather will be) a toy implementation of MapReduce in Go. While it
may be somewhat useful, it is intended mostly to be educational, as there will
be several places with inefficiencies, and severely lacking in features.

If you are curious about the name Goomba: I had a reason at some point, but
have since forgotten the tenuous connection between the famous Mario adversary
and MapReduce. Rest assured that the name was very clever and witty...
