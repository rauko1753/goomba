package mr

import (
	"net/rpc"
	"path/filepath"
	"sync"
)

type Master struct {
	Workers chan *rpc.Client
}

func (m *Master) RegisterWorker(workerURL string, _ *struct{}) error {
	m.Workers <- getClient(workerURL)
	return nil
}

func runMaster() {
	master := &Master{make(chan *rpc.Client)}
	listener := getListener(master, *masterURL)

	// register workers
	go serveRPC(listener)

	// setup master bookkeeping
	input := globInput()
	numparts := len(input)
	wg := sync.WaitGroup{}
	inter := make([][]string, numparts)

	// run map tasks
	for _, file := range input {
		task := MapTask{file, numparts}
		worker := <-master.Workers

		wg.Add(1)
		go func() {
			var parts []string
			makeCall(worker, "Worker.MapTask", task, &parts)

			for index, part := range parts {
				inter[index] = append(inter[index], part)
			}

			wg.Done()
			master.Workers <- worker
		}()
	}
	wg.Wait()

	// run reduce tasks
	output := make([]string, numparts)
	for index, files := range inter {
		task := ReduceTask{files}
		worker := <-master.Workers

		wg.Add(1)
		go func(index int) {
			makeCall(worker, "Worker.ReduceTask", task, &output[index])
			wg.Done()
			master.Workers <- worker
		}(index)
	}
	wg.Wait()

	// clean up workers
}

func globInput() []string {
	input, err := filepath.Glob(*inputPattern)
	if err != nil {
		panic(err)
	}
	return input
}
