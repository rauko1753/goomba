package mr

import (
	"encoding/gob"
	"io"
	"io/ioutil"
	"os"
)

type Worker struct {
	Program
}

func (w *Worker) MapTask(task MapTask, interFiles *[]string) error {
	input := w.Inputer.Read(task.Filename)
	inter := w.Mapper.Map(input)
	parts := w.Parter.Partition(inter, task.NumParts)
	w.Comparator.Sort(parts)
	*interFiles = writeInter(parts)
	return nil
}

func (w *Worker) ReduceTask(task ReduceTask, outputFilename *string) error {
	inter := make([]*PeekChanKeyValue, len(task.Filenames))
	for index, filename := range task.Filenames {
		inter[index] = readInter(filename)
	}

	outfile, err := ioutil.TempFile(".", "mrout-")
	if err != nil {
		panic(err)
	}
	output := make(chan KeyValue)

	go func() {
		for _, part := range inter {
			for true {
				kv, ok := part.Read()
				if !ok {
					break
				}

				kvs := KeyValues{kv.Key, make(chan string)}
				go func() {
					kvs.Values <- kv.Value
					for index := 0; index < len(inter); index++ {
						kv, ok := inter[index].Peek()
						for ok && kv.Key == kvs.Key {
							inter[index].Read()
							kvs.Values <- kv.Value
							kv, ok = inter[index].Peek()
						}
					}
					close(kvs.Values)
				}()

				kvsout := make(chan string)
				go func() {
					w.Reducer(kvs, kvsout)
					close(kvsout)
				}()

				for value := range kvsout {
					output <- KeyValue{kvs.Key, value}
				}
			}
		}
		close(output)
	}()

	w.Outputer(outfile, output)
	*outputFilename = outfile.Name()

	return nil
}

func runWorker(p Program) {
	worker := &Worker{p}
	listener := getListener(worker, *workerURL)
	master := getClient(*masterURL)
	makeCall(master, "Master.RegisterWorker", *workerURL, nil)
	serveRPC(listener)
}

func writeInter(parts [][]KeyValue) []string {
	files := make([]string, len(parts))
	for index, part := range parts {
		file, err := ioutil.TempFile(".", "mr-")
		if err != nil {
			panic(err)
		}

		encoder := gob.NewEncoder(file)
		for _, kv := range part {
			if err := encoder.Encode(kv); err != nil {
				panic(err)
			}
		}
		if err := file.Close(); err != nil {
			panic(err)
		}

		files[index] = file.Name()
	}
	return files
}

type PeekChanKeyValue struct {
	in   chan KeyValue
	head KeyValue
	ok   bool
}

func (c *PeekChanKeyValue) Peek() (v KeyValue, ok bool) {
	if !c.ok {
		c.head, c.ok = <-c.in
	}
	return c.head, c.ok
}

func (c *PeekChanKeyValue) Read() (v KeyValue, ok bool) {
	if c.ok {
		c.ok = false
		return c.head, true
	}
	v, ok = <-c.in
	return v, ok
}

func readInter(filename string) *PeekChanKeyValue {
	inter := make(chan KeyValue)
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	dec := gob.NewDecoder(file)
	go func() {
		for {
			var kv KeyValue
			if err := dec.Decode(&kv); err != nil {
				if err == io.EOF {
					break
				} else {
					panic(err)
				}
			}
			inter <- kv
		}
		close(inter)
	}()
	return &PeekChanKeyValue{in: inter}
}
