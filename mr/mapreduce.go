package mr

import (
	"fmt"
	"hash/fnv"
	"io"
	"io/ioutil"
	"os"
	"sort"
)

type KeyValue struct {
	Key   string
	Value string
}

type KeyValues struct {
	Key    string
	Values chan string
}

type Inputer func(r io.Reader, input chan KeyValue)

func (i Inputer) Read(filename string) chan KeyValue {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	input := make(chan KeyValue)
	go func() {
		i(file, input)
		close(input)
	}()
	return input
}

func defaultInputer(r io.Reader, input chan KeyValue) {
	text, err := ioutil.ReadAll(r)
	if err != nil {
		panic(err)
	}
	input <- KeyValue{"", string(text)}
}

type Mapper func(input KeyValue, inter chan KeyValue)

func (m Mapper) Map(input chan KeyValue) chan KeyValue {
	inter := make(chan KeyValue)
	go func() {
		for kv := range input {
			m(kv, inter)
		}
		close(inter)
	}()
	return inter
}

type Parter func(key string, numparts int) int

func (p Parter) Partition(inter chan KeyValue, numparts int) [][]KeyValue {
	partitions := make([][]KeyValue, numparts)
	for kv := range inter {
		index := p(kv.Key, numparts)
		partitions[index] = append(partitions[index], kv)
	}
	return partitions
}

func defaultParter(key string, numparts int) int {
	hash := fnv.New64()
	io.WriteString(hash, key)
	return int(hash.Sum64() % uint64(numparts))
}

type Comparator func(key1, key2 string) bool

func (c Comparator) Sort(parts [][]KeyValue) {
	for _, part := range parts {
		KeyValueSorter{part, c}.Sort()
	}
}

func defaultComparator(key1, key2 string) bool {
	return key1 < key2
}

type Reducer func(inter KeyValues, output chan string)

type Outputer func(w io.Writer, output chan KeyValue)

func defaultOutputer(w io.Writer, output chan KeyValue) {
	for kv := range output {
		fmt.Fprintf(w, "%s: %s\n", kv.Key, kv.Value)
	}
}

type MapTask struct {
	Filename string
	NumParts int
}

type ReduceTask struct {
	Filenames []string
}

type KeyValueSorter struct {
	Slice      []KeyValue
	Comparator func(k1, k2 string) bool
}

func (s KeyValueSorter) Len() int {
	return len(s.Slice)
}

func (s KeyValueSorter) Less(i, j int) bool {
	return s.Comparator(s.Slice[i].Key, s.Slice[j].Key)
}

func (s KeyValueSorter) Swap(i, j int) {
	s.Slice[i], s.Slice[j] = s.Slice[j], s.Slice[i]
}

func (s KeyValueSorter) Sort() {
	sort.Sort(s)
}
