package mr

import (
	"net"
	"net/rpc"
)

func getListener(rcvr interface{}, laddr string) net.Listener {
	rpc.Register(rcvr)
	listener, err := net.Listen("tcp", laddr)
	if err != nil {
		panic(err)
	}
	return listener
}

func getClient(addr string) *rpc.Client {
	client, err := rpc.Dial("tcp", addr)
	if err != nil {
		panic(err)
	}
	return client
}

func makeCall(c *rpc.Client, method string, args, reply interface{}) {
	if err := c.Call(method, args, reply); err != nil {
		panic(err)
	}
}

func serveRPC(l net.Listener) {
	for {
		if conn, err := l.Accept(); err != nil {
			panic(err)
		} else {
			go rpc.ServeConn(conn)
		}
	}
}
