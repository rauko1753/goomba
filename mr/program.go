package mr

import (
	"flag"
)

var (
	masterURL     = flag.String("mr-master", "", "URL of the master")
	workerURL     = flag.String("mr-worker", "", "URL of this worker")
	inputPattern  = flag.String("mr-input", "", "Input file pattern")
	outputPattern = flag.String("mr-output", "", "Output file pattern")
)

type Program struct {
	Mapper
	Reducer
	Inputer
	Parter
	Comparator
	Outputer
}

func (p Program) Run() {
	if !flag.Parsed() {
		flag.Parse()
	}

	if p.Inputer == nil {
		p.Inputer = defaultInputer
	}
	if p.Parter == nil {
		p.Parter = defaultParter
	}
	if p.Comparator == nil {
		p.Comparator = defaultComparator
	}
	if p.Outputer == nil {
		p.Outputer = defaultOutputer
	}

	if *masterURL == *workerURL {
		runMaster()
	} else {
		runWorker(p)
	}
}
